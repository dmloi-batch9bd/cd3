*** Settings ***
Library    Selenium2Library
Library    String
Resource    D:/DataStudies/CD3/TestWeb/Locator/Locator.robot
Resource    D:/DataStudies/CD3/TestWeb/VariablesData/Variables.robot
Documentation    Apply discount code
Test Teardown    Close Browser
Test Timeout    2 minutes
*** Test Cases ***
Successfully applied discount code with orders with 2 products IPHONE and SAMSUNG
    [Documentation]    Successfully applied discount code with orders with 2 products IPHONE and SAMSUNG
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click the "Mobile" menu
    ...    - 3. At the IPHONE product click "Add to cart"
    ...    - 4. At the cart click "CONTINUE SHOPPING"
    ...    - 5. At SAMSUNG product click "Add to cart"
    ...    - 6. Enter the code "GURU50" in the "Discount Codes" field
    ...    - 7. Click "APPLY"
    [Tags]    TG6_TC_01
    Open Browser and go to web
    Click on Mobile menu
    Add IPHONE to cart and Add SAMSUNG GALAXY to cart
    Input coupon GURU50 (5%)
    Element Should Contain    ${MessageCart}    Coupon code "GURU50" was applied.
    Element Should Contain    ${DiscountCode}    DISCOUNT (GURU50)
    Element Should Be Visible    ${DiscountCode}
    Set Test Message    Displays the message "Coupon code GURU50 was applied." and show "DISCOUNT(GURU50)"
    Capture Page Screenshot    Screenshot/TG_5/check1.png
Verify percentage of discount code GURU50
    [Documentation]    Verify percentage of discount code GURU50
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click the "Mobile" menu
    ...    - 3. At the IPHONE product click "Add to cart"
    ...    - 4. At the cart click "CONTINUE SHOPPING"
    ...    - 5. At SAMSUNG product click "Add to cart"
    ...    - 6. Enter the code "GURU50" in the "Discount Codes" field
    ...    - 7. Click "APPLY"
    [Tags]    TG6_TC_02
    Open Browser and go to web
    Click on Mobile menu
    Add IPHONE to cart and Add SAMSUNG GALAXY to cart
    Input coupon GURU50 (5%)
    Element Should Contain    ${MessageCart}    Coupon code "GURU50" was applied.
    Element Should Contain    ${DiscountCode}    DISCOUNT (GURU50)
    Element Should Be Visible    ${DiscountCode}
    Verify discount value base on coupon code
    Should Be True    "${discount_value}"=="5.0"
    Set Test Message    Displays the message "Coupon code "GURU50" was applied." and show "DISCOUNT(GURU50)". Shopping cart 5% off
    Capture Page Screenshot    Screenshot/TG_5/check2.png
Cancel the application of the discount code successfully
    [Documentation]    Cancel the application of the discount code successfully
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click the "Mobile" menu
    ...    - 3. At the IPHONE product click "Add to cart"
    ...    - 4. At the cart click "CONTINUE SHOPPING"
    ...    - 5. At SAMSUNG product click "Add to cart"
    ...    - 6. Enter the code "GURU50" in the "Discount Codes" field
    ...    - 7. Click "APPLY"
    ...    - 8. Click "CANCEL"
    [Tags]    TG6_TC_03
    Open Browser and go to web
    Click on Mobile menu
    Add IPHONE to cart and Add SAMSUNG GALAXY to cart
    Input coupon GURU50 (5%)
    Element Should Contain    ${MessageCart}    Coupon code "GURU50" was applied.
    Element Should Contain    ${DiscountCode}    DISCOUNT (GURU50)
    Element Should Be Visible    ${DiscountCode}
    Click Element    ${BtnCancel}
    Element Should Contain    ${MessageCart}    Coupon code was canceled.
    Element Should Not Be Visible    ${DiscountCode}
    Set Test Message    Display the message "Coupon code was canceled." and "DISCOUNT(GURU50)" is not displayed.
    Capture Page Screenshot    Screenshot/TG_5/check3.png
*** Keywords ***
Open Browser and go to web
    Open Browser    ${Browser}    ${Device}
Click on Mobile menu
    Click Link    ${Mobile_menu}
Add IPHONE to cart and Add SAMSUNG GALAXY to cart
    Click Element    ${BtnAddIphone}
    Click Button    ${BtnContinue}
    Click Button    ${BtnAddSamsung}
Input coupon GURU50 (5%)
    Input Text    ${CodeCart}    ${TextCode}
    Click Element    ${BtnApplyCode}
Verify discount value base on coupon code
    ${number_Subtotal}    Get Text    ${NumberSubtotal}
    ${number_Discount}    Get Text    ${NumberDiscount}
    ${txt_Subtotal}    Fetch From Right    ${number_Subtotal}   $
    ${txt_Discount}    Fetch From Right    ${number_Discount}    $
    ${discount_value}=   Evaluate    ${txt_Discount}/${txt_Subtotal}*100
    Set Suite Variable    ${discount_value}

