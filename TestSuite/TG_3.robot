*** Settings ***
Library    Selenium2Library
Resource    D:/DataStudies/CD3/TestWeb/Locator/Locator.robot
Resource    D:/DataStudies/CD3/TestWeb/VariablesData/Variables.robot
Documentation    Create Account
Test Teardown    Close Browser
Test Timeout    2 minutes
*** Test Cases ***
Enter the correct information in the fields
    [Documentation]    Enter the correct information in the fields
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account", select "Register" function
    ...    - 3. Enter First Name, Middle Name/Initial, Last Name, Email Address, Password, Confirm Password.
    ...    - 4. Click on "Sign Up for Newsletter"
    ...    - 5. Click "Register"
    [Tags]    TG3_TC_01
    Open Browser and go to web
    Go to the Register page
    Correctly enter all fields
    Click CheckBox Sign Up for Newsletter
    Click Register
    Title Should Be    My Account
    Element Should Contain    ${Meassage}    Thank you for registering with Main Website Store.
    Element Should Be Visible    ${Meassage}
    Set Test Message    Display the message "Thank you for registering with Main Website Store."
    Capture Page Screenshot    Screenshot/TG_3/check1.png
Do not enter information in the fields
    [Documentation]    Do not enter information in the fields
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account", select "Register" function
    ...    - 3. Click "Register"
    [Tags]    TG3_TC_02
    Open Browser and go to web
    Go to the Register page
    Click Register
    Check Display the message "This is a required field."
    Set Test Message    Display the message "This is a required field."
    Capture Page Screenshot    Screenshot/TG_3/check2.png
Enter information in one field, the remaining fields are left blank
    [Documentation]    Enter information in one field, the remaining fields are left blank
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account", select "Register" function
    ...    - 3. Enter First Name
    [Tags]    TG3_TC_03
    Open Browser and go to web
    Go to the Register page
    Input Text    ${InputFirstName}    ${TextFirstName}
    Click Register
    Check Display the message "This is a required field."
    Set Test Message    Display the message "This is a required field."
    Capture Page Screenshot    Screenshot/TG_3/check3.png
*** Keywords ***
Open Browser and go to web
    Open Browser    ${Browser}    ${Device}
Go to the Register page
    Click Element    ${MenuAccount}
    Click Element    ${MenuRegister}
Correctly enter all fields
    Input Text    ${InputFirstName}    ${TextFirstName}
    Input Text    ${InputMiddleName/Initial}    ${TextMiddleName/Initial}
    Input Text    ${InputLastName}    ${TextLastName}
    Input Text    ${InputEmailAddress}    ${TextEmailAddress}
    Input Password    ${InputPassword}    ${TextPassword}
    Input Password    ${InputConfirmPassword}    ${TextPassword}
Click Register
    Click Button    ${ButtonRegister}
Click CheckBox Sign Up for Newsletter
    Click Button    ${CheckBoxSignUpforNewsletter}
Check Display the message "This is a required field."
    Element Should Be Visible    ${MeassageRequiredField}
    Element Should Contain    ${MeassageRequiredField}    This is a required field.


