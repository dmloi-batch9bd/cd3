*** Settings ***
Library    Selenium2Library
Resource    D:/DataStudies/CD3/TestWeb/Locator/Locator.robot
Resource    D:/DataStudies/CD3/TestWeb/VariablesData/Variables.robot
Documentation    Log out
Test Teardown    Close Browser
Test Timeout    2 minutes
*** Test Cases ***
Successful log out
    [Documentation]    Successful log out
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account" and then click "Log in"
    ...    - 3. Enter the correct email address
    ...    - 4. Enter the correct password
    ...    - 5. Click the "login" button
    ...    - 6. Click "Account" and then click "Log out"
    [Tags]    TG2_TC_01
    Open Browser and go to web
    Go to the login page
    Input Text    ${InputEmail}    ${EmailText}
    Input Password    ${InputPass}    ${PasswordText}
    Click button login
    Title Should Be    My Account
    Element Should Contain    ${ElementPageMyAccount}    MY DASHBOARD
    Click Element    ${MenuAccount}
    Click Element    ${MenuLogout}
    Element Should Be Visible    ${ElementTitlePage}
    Element Should Contain    ${ElementTitlePage}    YOU ARE NOW LOGGED OUT
    Element Should Contain    ${ElementMess}    You have logged out and will be redirected to our homepage in 5 seconds.
    Wait Until Page Contains    This is demo site for
    Title Should Be    Home page
    Set Test Message    Sign out successfully and display the message "You are logged out and will be redirected to our homepage in 5 seconds"
    Capture Page Screenshot    Screenshot/TG_2/check1.png
*** Keywords ***
Open Browser and go to web
    Open Browser    ${Browser}    ${Device}
Go to the login page
    Click Element    ${MenuAccount}
    Click Element    ${MenuLogin}
Click button login
    Click Button    ${ButtonLogin}



