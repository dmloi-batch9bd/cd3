*** Settings ***
Library    Selenium2Library
Library    String
Resource    D:/DataStudies/CD3/TestWeb/Locator/Locator.robot
Resource    D:/DataStudies/CD3/TestWeb/VariablesData/Variables.robot
Documentation    Add products of Mobile menu to cart
Test Teardown    Close Browser
Test Timeout    2 minutes
*** Test Cases ***
Add 1 Product
    [Documentation]    Add 1 Product
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click the "Mobile" menu
    ...    - 3. At the IPHONE product click "Add to cart"
    [Tags]    TG5_TC_01
    Open Browser and go to web
    Click on Mobile menu
    Click Element    ${BtnAddIphone}
    Element Should Contain    ${MessageCart}    IPhone was added to your shopping cart.
    Element Should Contain    ${NumberCart}    1
    Set Test Message    Display the message "IPhone was added to your shopping cart." and the cart goes up 1
    Capture Page Screenshot    Screenshot/TG_4/check1.png
Add 2 Product
    [Documentation]    Add 2 Product
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click the "Mobile" menu
    ...    - 3. At the IPHONE product click "Add to cart"
    ...    - 4. At the cart click "CONTINUE SHOPPING"
    ...    - 5. At SAMSUNG product click "Add to cart"
    [Tags]    TG5_TC_02
    Open Browser and go to web
    Click on Mobile menu
    Add IPHONE to cart and Add SAMSUNG GALAXY to cart
    Element Should Contain    ${MessageCart}    Samsung Galaxy was added to your shopping cart.
    Element Should Contain    ${NumberCart}    2
    Set Test Message    Display the message "Samsung Galaxy was added to your shopping cart." and the cart goes up 2
    Capture Page Screenshot    Screenshot/TG_4/check2.png
Add 2 products IPHONE, SAMSUNG and change the number of SAMSUNG products to 2
    [Documentation]    Add 2 products IPHONE, SAMSUNG and change the number of SAMSUNG products to 2
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click the "Mobile" menu
    ...    - 3. At the IPHONE product click "Add to cart"
    ...    - 4. At the cart click "CONTINUE SHOPPING"
    ...    - 5. At SAMSUNG product click "Add to cart"
    ...    - 6. In the QTY column of the SAMSUNG product, click "Edit"
    ...    - 7. Enter the value 2 in the "QTY" field
    ...    - 8. Click "Update"
    [Tags]    TG5_TC_03
    Open Browser and go to web
    Click on Mobile menu
    Add IPHONE to cart and Add SAMSUNG GALAXY to cart
    Update quantity to 2 for SAMSUNG GALAXY
    Element Should Contain    ${MessageCart}    Samsung Galaxy was updated in your shopping cart.
    Element Should Contain    ${NumberCart}    3
    Set Test Message    Display the message "Samsung Galaxy was updated in your shopping cart." and the cart goes up 3
    Capture Page Screenshot    Screenshot/TG_4/check3.png
*** Keywords ***
Open Browser and go to web
    Open Browser    ${Browser}    ${Device}
Click on Mobile menu
    Click Link    ${Mobile_menu}
Add IPHONE to cart and Add SAMSUNG GALAXY to cart
    Click Element    ${BtnAddIphone}
    Click Button    ${BtnContinue}
    Click Button    ${BtnAddSamsung}
Update quantity to 2 for SAMSUNG GALAXY
    Click Element    ${EditNumberSamsung}
    Clear Element Text    ${ImputNumber}
    Input Text    ${ImputNumber}    2
    Click Button    ${ButtonUpdateCart}

