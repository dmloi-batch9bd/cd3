*** Settings ***
Library    Selenium2Library
Resource    D:/DataStudies/CD3/TestWeb/Locator/Locator.robot
Resource    D:/DataStudies/CD3/TestWeb/VariablesData/Variables.robot
Documentation    Edit personal information
Test Teardown    Close Browser
Test Timeout    2 minutes
*** Test Cases ***
Edit information successfully. Do not change the old password.
    [Documentation]    Edit information successfully. Do not change the old password.
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account" and then click "Log in"
    ...    - 3. Enter the correct email address
    ...    - 4. Enter the correct password
    ...    - 5. Click the "login" button
    ...    - 6. Then click "Account Information" button
    ...    - 7. Enter information for fields that need "Edit"
    ...    - 8. Click the "Save" button
    [Tags]    TG4_TC_01
    Open Website
    Login Account with "thanhthuy12@gmail.com" and "Thuy12" account
    Edit Account with no password change
    Check Edit information successfully
    Capture Page Screenshot    Screenshot/TG_4/check1.png
Edit information successfully. Change old password
    [Documentation]    Edit information successfully. Change old password
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account" and then click "Log in"
    ...    - 3. Enter the correct email address
    ...    - 4. Enter the correct password
    ...    - 5. Click the "login" button
    ...    - 6. Then click "Account Information" button
    ...    - 7. Enter information for fields that need "Edit"
    ...    - 8. Click the "Change Password" button
    ...    - 9. Enter new password and re-enter new password
    ...    - 10. Nhấp vào nút "Save"
    [Tags]    TG4_TC_02
    Open Website
    Login Account with "thanhthuy12@gmail.com" and "Thuy12" account
    Edit Account with password change
    Check Edit information successfully
    Capture Page Screenshot    Screenshot/TG_4/check2.png
*** Keywords ***
Open Website
    Open browser               ${Browser}    ${Device}
    Maximize Browser Window
    
Login Account with "${emailValue}" and "${passwordValue}" account
    Click Element    ${account} 
    Click Element    ${loginOption}
    Input Text       ${emailAddressInput}    ${emailValue}
    Input Text       ${passwordInput}        ${passwordValue}
    Click Element    ${loginButton}
Edit Account with no password change
    click Element    ${edit}
    Input Text       ${firstNameInp}       ${editFirstNameText}
    Input Text       ${middleNameInp}      ${editMiddleNameText}
    Input Text       ${lastNameInp}        ${editLastNameText}
    Input Text       ${emailAddressInp}    ${editEmailAddressText}
    Input Password       ${passwordInp}        ${passwordTextEdit}
    Click Element    ${saveButton}
Edit Account with password change
    click Element    ${edit}
    Input Text       ${firstNameInp}              ${editFirstNameText}
    Input Text       ${middleNameInp}             ${editMiddleNameText}
    Input Text       ${lastNameInp}               ${editLastNameText}
    Input Text       ${emailAddressInp}           ${editEmailAddressText}
    Input Text       ${passwordInp}               ${passwordTextEdit}
    Click Element    ${changePasswordCheckbox}
    Input Text       ${newPasswordInp}            ${newPasswordText}
    Input Text       ${confirmNewPasswordInp}     ${confirmNewPasswordText}
    Click Element    ${saveButton}
Check Edit information successfully
    Element Should Contain    ${Meassage}    The account information has been saved.
    Element Should Be Visible    ${Meassage}


