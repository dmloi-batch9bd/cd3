*** Settings ***
Library    Selenium2Library
Resource    D:/DataStudies/CD3/TestWeb/Locator/Locator.robot
Resource    D:/DataStudies/CD3/TestWeb/VariablesData/Variables.robot
Documentation    Log in
Test Teardown    Close Browser
Test Timeout    2 minutes
*** Test Cases ***
Logged in successfully
    [Documentation]    Logged in successfully
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account" and then click "Log in"
    ...    - 3. Enter the correct email address
    ...    - 4. Enter the correct password
    ...    - 5. Click the "login" button
    [Tags]    TG1_TC_01
    Open Browser and go to web
    Go to the login page
    Enter your email address
    Enter password
    Click button login
    Title Should Be    My Account
    Element Should Contain    ${ElementPageMyAccount}    MY DASHBOARD
    Set Test Message    Login successfully and redirect to "My Dashboard" page
    Capture Page Screenshot    Screenshot/TG_1/check1.png
Do not enter the account name
    [Documentation]    Do not enter the account name
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account" and then click "Log in"
    ...    - 3. Enter the correct password
    ...    - 4. Click the "login" button
    [Tags]    TG1_TC_02
    Open Browser and go to web
    Go to the login page
    Enter password
    Click button login
    Check Display the message "This is a required field."
    Set Test Message    Login failed and showing "username is required field" message
    Capture Page Screenshot    Screenshot/TG_1/check2.png
Do not enter password
    [Documentation]    Do not enter password
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account" and then click "Log in"
    ...    - 3. Enter your email address
    ...    - 4. Click the "login" button
    [Tags]    TG1_TC_03
    Open Browser and go to web
    Go to the login page
    Enter your email address
    Click button login
    Check Display the message "This is a required field."
    Set Test Message    Login failed and showing "username is required field" message
    Capture Page Screenshot    Screenshot/TG_1/check3.png
Do not enter username and password
    [Documentation]    Do not enter password
    ...    - 1. Go to the website "http://live.techpanda.org/index.php/"
    ...    - 2. Click "Account" and then click "Log in"
    ...    - 3. Click the "login" button
    [Tags]    TG1_TC_04
    Open Browser and go to web
    Go to the login page
    Click button login
    Check Display the message "This is a required field."
    Set Test Message    Login failed and showing "username is required field" message
    Capture Page Screenshot    Screenshot/TG_1/check4.png
*** Keywords ***
Open Browser and go to web
    Open Browser    ${Browser}    ${Device}
Go to the login page
    Click Element    ${MenuAccount}
    Click Element    ${MenuLogin}
Enter your email address
    Input Text    ${InputEmail}    ${EmailText}
Enter password
    Input Password    ${InputPass}    ${PasswordText}
Click button login
    Click Button    ${ButtonLogin}
Check Display the message "This is a required field."
    Wait Until Element Is Visible    ${MeassageRequiredField}
    Element Should Be Visible    ${MeassageRequiredField}
    Element Should Contain    ${MeassageRequiredField}    This is a required field.


