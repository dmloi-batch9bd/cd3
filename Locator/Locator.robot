*** Settings ***
Library    Selenium2Library
*** Variables ***
# Locator TG_1
${MenuAccount}    //a[@class="skip-link skip-account"]
${MenuLogin}    //a[@title="Log In"]
${InputEmail}    //input[@id="email"]
${InputPass}    //input[@id="pass"]
${ButtonLogin}    //button[@type="submit" and @class="button" and @name="send"]
${ElementPageMyAccount}    //div[@class="page-title"]
${MenuLogout}    //a[@title="Log Out"]
# Locator TG_2
${ElementTitlePage}    //div[@class="page-title"]
${ElementMess}    //div[@class="col-main"]//child::p
# Locator TG_3
${MenuRegister}    //a[@title="Register"]
${InputFirstName}    //input[@id="firstname"]
${InputMiddleName/Initial}    //input[@id="middlename"]
${InputLastName}    //input[@id="lastname"]
${InputEmailAddress}    //input[@id="email_address"]
${InputPassword}    //input[@id="password"]
${InputConfirmPassword}    //input[@id="confirmation"]
${CheckBoxSignUpforNewsletter}    //input[@id="is_subscribed"]
${ButtonRegister}    //button[@title="Register" and @class="button"]
${Meassage}    //ul[@class="messages"]
${MeassageRequiredField}    //div[@class="validation-advice"]
# Locator TG_4
${account}                   //div[@class="account-cart-wrapper"]
${emailAddressInput}         //input[@id="email"]
${passwordInput}             //input[@id="pass"]
${loginButton}               //button[@id='send2']
${loginOption}               //div[@class="links"]//li/a[contains(text(), 'Log In')]
${edit}                      //h3[contains(text(),'Contact Information')]//following-sibling::a
${firstNameInp}              //input[@id="firstname"]
${middleNameInp}             //input[@id="middlename"]
${lastNameInp}               //input[@id="lastname"]
${emailAddressInp}           //input[@id="email"]
${passwordInp}               //input[@id="current_password"]
${changePasswordCheckbox}    //input[@id="change_password"]
${newPasswordInp}            //input[@id="password"]
${confirmNewPasswordInp}     //input[@id="confirmation"]                                                                                              
${saveButton}                //button[@class="button"]

# Locator TG_5
${MessageCart}    //li[@class="success-msg"]//child::span
${NumberCart}    //span[@class="count"]
${Mobile Menu}    //a[text()="Mobile"]
${BtnAddIphone}    //*[text()="IPhone"]//following::button[1]
${BtnContinue}    //*[@class="button2 btn-continue"]
${BtnAddSamsung}    //*[text()="Samsung Galaxy"]//following::button[1]
${EditNumberSamsung}    //a[text()="Samsung Galaxy"]//following::td[15]//child::ul
${ImputNumber}    //div[@class="qty-wrapper"]//child::input
${ButtonUpdateCart}    //button[@class="button btn-cart"]
${CodeCart}    //input[@name="coupon_code"]
${BtnApplyCode}    //button[@class="button2" and @value="Apply"]
${DiscountCode}    //td[contains(text(),'Subtotal')]//following::td[2]
# Locator TG_6
${NumberSubtotal}    //td[contains(text(),'Subtotal')]//following::span[1]
${NumberDiscount}    //td[contains(text(),'Discount')]//following::span[1]
${BtnCancel}    //button[@title="Cancel"]

